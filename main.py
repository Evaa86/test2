from math import sqrt


class Calculator:

  def __init__(self):
    pass

  def add(self, x1, x2):
    return x1 + x2

  def multiply(self, x1, x2):
    return x1 * x2

  def subtract(self, x1, x2):
    return x1 - x2

  def divide(self, x1, x2):
    if x2 != 0:
      return x1/x2

  def root(self, x1):
    return sqrt(x1)

  def power(self, x1, x2):
    return x1 ** x2

  def ostatok(self, x1, x2):
    return  x1 % x2